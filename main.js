
$(document).ready(function() {

    // Question 1
    $("#questionOne").click(function() {
        var url = 'http://private-76432-jobadder1.apiary-mock.com/jobs';
        getResults(url, 'GET', function(resp) {
            for (val in resp) {
                if (resp[val].jobId == 7) {
                    document.getElementById('displayResults').innerHTML = JSON.stringify(resp[val]);
                    break;
                };
            };
        });
    });

    // Question 2
    $("#questionTwo").click(function() {
        var url = 'http://private-76432-jobadder1.apiary-mock.com/candidates';
        getResults(url, 'GET', function(resp) {
            var name = "";
            for (val in resp) {
                if (resp[val].name[0].toLowerCase() == "b") {
                    name += "Name: " + resp[val].name + "<br/>"
                }
            }
            document.getElementById('displayResults').innerHTML = name;
        });
    });

    // Question 3
    $("#questionThree").click(function() {
        var url = 'http://private-76432-jobadder1.apiary-mock.com/candidates';
        getResults(url, 'GET', function(resp) {
            var num = 0;
            for (val in resp) {
                if (resp[val].skillTags.toLowerCase().includes('carpentry')) {
                    num += 1;
                }
            }
            document.getElementById('displayResults').innerHTML = `There are ${num} candidates with Skill Tag 'carpentry'.`;
        });
    });

    function getResults(requestURL, method, callback){
        $.ajax({
            url:requestURL,
            method:method,
            dataType:"json",
            success:callback,
            error: function(reason, xhr) {
                document.getElementById('displayResults').innerHTML = "Error Encountered - " + reason;
            }
        });
    }
});

